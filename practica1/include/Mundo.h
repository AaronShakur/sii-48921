// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#define TAM 1024

#include <vector>
#include "Plano.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "DatosMemCompartida.h"//Incluye  Raqueta.h

class CMundo  
{
public:
	
	CMundo();
	virtual ~CMundo();	

	char men[TAM]; //PUNTO 3
	int fifo;	//PUNTO 3
	int fichero;
	int f;
	
	void Init();

		
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	bool Fin();
	
	DatosMemCompartida datos; //PUNTO 2
	DatosMemCompartida *dat; //PUNTO 2

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;


	//DISPARO
	//Disparo disparo;

	int puntos1;
	int puntos2;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
