#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <error.h>

#include <stdlib.h>

#define MAX_BUF 1024

int main(void) 
{
	
	char mensaje[MAX_BUF]; //cadena de caracteres
	
	mkfifo("/tmp/tuberia",0777); /*Se crea la tubería*/
	int t=open("/tmp/tuberia", O_RDONLY); /*Se abre la tubería en modo lectura*/
	
	int jugador, puntos;
	jugador = 0; puntos = 0;	

	//Lee un bucle infinito 
	while(1)
	{
		read(t, mensaje, sizeof(mensaje));

		if (jugador == (int) mensaje[8] && puntos == (int) mensaje[45])
			break;
		jugador = (int) mensaje[8];
		puntos = (int) mensaje[45];

		printf("%d\n",t);		
		printf("Recibido el mensaje: %s\n", mensaje); //Escribe que lo he leido
	}
	
	close (t); /*Se cierra la tubería*/
	unlink("/tmp/tuberia"); /*Se borra la tuberia*/
	
	return 0;
}
