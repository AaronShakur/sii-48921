// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
#include "stdlib.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=4;
	velocidad.y=4;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	
	glColor3ub(255,128,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
		
	glPopMatrix();

	
}

void Esfera::Mueve(float t)
{
centro=centro+velocidad*t;

//Esfera pulsante
radio+=0.001f;
if(radio>1) radio=0.1f;


}
